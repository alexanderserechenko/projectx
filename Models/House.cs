﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectXApi.Models
{
    public class House
    {
        [Key]
        public int HouseId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool Available { get; set; }

        //[ForeignKey("ImageId")]
        //public int? ImageId { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public double Price { get; set; }

    }
}
