﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using MimeKit;
using ProjectXApi.Interfaces;
using ProjectXApi.Models;
using System;
using System.Threading.Tasks;


namespace ProjectXApi.Services
{



    public class EmailSender : Interfaces.IEmailSender
    {
        private readonly Contact _contact;
        private readonly IHostingEnvironment _env;

        public EmailSender(
            IOptions<Contact> contact,
            IHostingEnvironment env)
        {
            _contact = contact.Value;
            _env = env;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            try
            {
                var mimeMessage = new MimeMessage();

                //mimeMessage.From.Add(new MailboxAddress(_contact.Name, _contact.Email));
                mimeMessage.From.Add(new MailboxAddress("projectxcrax@gmail.com"));

                mimeMessage.To.Add(new MailboxAddress("alexander.serechenko@gmail.com"));
                mimeMessage.Subject = subject;
                mimeMessage.Body = new TextPart("html")
                {
                    
                    Text = "Email: " + email + " Bericht: " + message
                     
                };

                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (_env.IsDevelopment())
                    {
                        // The third parameter is useSSL (true if the client should make an SSL-wrapped
                        // connection to the server; otherwise, false).
                        await client.ConnectAsync("smtp.gmail.com", 465, true);
                    }
                    else
                    {
                        await client.ConnectAsync("smtp.gmail.com", 587, true);
                    }

                    await client.AuthenticateAsync("projectxcrax@gmail.com", "CraxitCraxit");


                    await client.SendAsync(mimeMessage);

                    await client.DisconnectAsync(true);
                }

            }
            catch (Exception ex)
            {
                // TODO: handle exception
                throw new InvalidOperationException(ex.Message);
            }
        }

    }

}
