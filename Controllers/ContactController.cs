﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using ProjectXApi.Data;
using ProjectXApi.Interfaces;
using ProjectXApi.Models;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectXApi.Controllers
{
    [Route("api/[controller]")]
    public class ContactController : Controller
    {
        private readonly Interfaces.IEmailSender _emailSender;
        private readonly ApplicationDbContext Db;

        public ContactController(ApplicationDbContext db, Interfaces.IEmailSender emailSender)
        {
            Db = db;
            _emailSender = emailSender;
        }

        [HttpGet("[action]")]
        public IActionResult GetContact()
        {
            return Ok(Db.Contacts.ToList());
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateContact([FromBody] Contact formdata)
        {
            if (ModelState.IsValid)
            {

                var newcontact = new Contact
                {
                    Name = formdata.Name,
                    Email = formdata.Email,
                    Message = formdata.Message,
                };



                await Db.Contacts.AddAsync(newcontact);

                await Db.SaveChangesAsync();

                var email = formdata.Email;
                var subject = formdata.Name;
                var message = formdata.Message;

                await _emailSender.SendEmailAsync( email,  subject,  message);
            }
            else
            {
                return BadRequest(ModelState);
            }


            return Ok(new JsonResult("Het bericht werd verstuurd"));
        }
    }
}
