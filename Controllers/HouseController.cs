﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProjectXApi.Data;
using ProjectXApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ProjectXApi.Controllers
{
    [Route("api/[controller]")]
    public class HouseController : Controller
    {

        private readonly ApplicationDbContext Db;

        public HouseController(ApplicationDbContext db)
        {
            Db = db;
        }


        [HttpGet("[action]")]
        public IActionResult GetHouses()
        {
            return Ok(Db.Houses.ToList());
        }

        [HttpPost("[action]")]
        [Authorize(Policy = "RequireAdministratorRole")]
        public async Task<IActionResult> AddHouse([FromBody] House formdata)
        {
            if (ModelState.IsValid)
            {
                
                var newhouse = new House
                {
                    Name = formdata.Name,
                    ImageUrl = formdata.ImageUrl,
                    Description = formdata.Description,
                    Available = formdata.Available,
                    Price = formdata.Price
                };



                await Db.Houses.AddAsync(newhouse);

                await Db.SaveChangesAsync();

            }
            else
            {
                return BadRequest(ModelState);
            }


            return Ok(new JsonResult("Het huis is toegevoegd"));
        }

        [HttpPut("[action]/{id}")]
        [Authorize(Policy = "RequireAdministratorRole")]

        public async Task<IActionResult> UpdateHouse([FromRoute] int id, [FromBody] House formdata)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var findHouse = Db.Houses.FirstOrDefault(p => p.HouseId == id);

            if (findHouse == null)
            {
                return NotFound();
            }

            // If the house was found
            findHouse.Name = formdata.Name;
            findHouse.Description = formdata.Description;
            findHouse.ImageUrl = formdata.ImageUrl;
            findHouse.Available = formdata.Available;
            findHouse.Price = formdata.Price;

            Db.Entry(findHouse).State = EntityState.Modified;

            await Db.SaveChangesAsync();

            return Ok(new JsonResult("Informatie is aangepast"));

        }


        [HttpDelete("[action]/{id}")]
        [Authorize(Policy = "RequireAdministratorRole")]

        public async Task<IActionResult> DeleteHouse([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // find the product

            var findProduct = await Db.Houses.FindAsync(id);

            if (findProduct == null)
            {
                return NotFound();
            }

            Db.Houses.Remove(findProduct);

            await Db.SaveChangesAsync();

            // Finally return the result to client
            return Ok(new JsonResult("Het huis werd verwijderd"));

        }



       








    }
}
