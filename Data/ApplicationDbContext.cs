﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjectXApi.Models;

namespace ProjectXApi.Data
{
    public class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<IdentityRole>().HasData(
                
                    new  { Id = "0", Name= "Admin", NormalizedName = "ADMIN"},
                    new  { Id = "1", Name= "Customer", NormalizedName = "CUSTOMER"}
                   
                );
        }

        public DbSet<House> Houses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        //public DbSet<Image> Images { get; set; }
        
    }
}
